Zip Arrays

Given 2 arrays of identical size you need to zip the two arrays together maintaining their order.

Example:

zipArrays([1,5,2], [3,6,4]) returns [1,3,5,6,2,4]

array_one = [1,5,2]
array_two = [3,6,4]
zipped_array = [1,3,5,6,2,4]
