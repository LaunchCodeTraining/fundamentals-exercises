Zip Arrays Expanded

Given any array of arrays (of the same size) you need to zip the arrays together maintaining their order.

Example:

zipArrays([[1,2,3],[7,8,9],["a","b","c"]]) returns [1,7,a,2,8,b,3,9,c]

array_one = [1,2,3]
array_two = [7,8,9]
array_three = [a,b,c]

zipped_array = [1,7,a,2,8,b,3,9,c]

Example:

zipArrays([[1,2,3],[4,5,6],["red","green","blue"],[0,0,0]]) returns [1,4,"red",0,2,5,"green",0,3,6,"blue",0]

array_one = [1,2,3]
array_two = [4,5,6]
array_three = ["red", "green", "blue"]
array_four = [0,0,0]

zipped_array = [1,4,"red",0,2,5,"green",0,3,6,"blue",0]
