Modify your algorithm from the previous exercise that will zip together any number of arrays with any lengths

Example:

zipArrays([[1,2,3],["horse","dog","cat","fly"],[15],[6,7,8,9,10,11]]) 

returns [1, "horse", 15, 6, 2, "dog", 7, 3, "cat", 8, "fly", 9, 10, 11]

array_one = [1,2,3]
array_two = ["horse", "dog", "cat", "fly"]
array_three = [15]
array_four = [6,7,8,9,10,11]

zipped_array = [1, "horse", 15, 6, 2, "dog", 7, 3, "cat", 8, "fly", 9, 10, 11]
